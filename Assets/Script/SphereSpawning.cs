using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereSpawning : MonoBehaviour
{
    public GameObject SphereObj;

    void OnMouseDown()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit))
        {
           GameObject newSphere = Instantiate(SphereObj, hit.point, Quaternion.identity);//��������� ��� � �����, � ��� ��� ������ ���� ��� ����, �������� ��� �� ������ �� ����
            newSphere.transform.Translate(new Vector3(0, 0.5f, 0));
            newSphere.transform.SetParent(transform);
        }
    }
}
