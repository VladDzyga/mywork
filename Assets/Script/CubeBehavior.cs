using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeBehavior : MonoBehaviour
{
    public GameObject Effect;
    private float speed = 0f; //��������
    private float turbo = 1f; //���������� ��� ������ ��������
    private float rotationSpeed = 0.1f; //�������� ��������

    void Start()
    {
        
    }
    void Update()
    {
        GameObject[] AllEnemies = GameObject.FindGameObjectsWithTag("CubeEnemy"); //������ �� ���� �� �������� � �����
        if (AllEnemies.Length != 0)
        {
            Transform NearEnemy = AllEnemies[0].transform; //������ ��������� ���, ���� ���� �� ����������
            float min = (AllEnemies[0].transform.position - transform.position).magnitude; //³������ �� �����
            for(int i = 1; i < AllEnemies.Length; i++) //������ ��� � �������� ������ ���������� �� ����, ���������� �� ������� ������
            {
                float DistanceToEnemy = (AllEnemies[i].transform.position - transform.position).magnitude;
                if (DistanceToEnemy < min)
                {
                    NearEnemy = AllEnemies[i].transform;
                    min = DistanceToEnemy;
                }
            }
            Vector3 VectorToEnemy = NearEnemy.position - transform.position;
            VectorToEnemy.y = 0;
            Quaternion targetRotation = Quaternion.LookRotation(VectorToEnemy);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationSpeed);//��������� ��� �� ���� � ������� �������� ��������
            if (speed <= 5 && VectorToEnemy.magnitude >= 1) {//���� ������� ������, � �������� �� �����������, �������������
                speed += 0.05f;
            }
            if(speed > 2 && VectorToEnemy.magnitude < 1.5) //���� �������� ������, � ������� ����, �������������
            {
                speed -= 0.1f;
            }
            if (AllEnemies.Length >= 30)//���� ������� ���� >= 30, ������ �������� �������� ������
            {
                turbo = 2;
            }
            else if (AllEnemies.Length >= 10)//���� ������� ���� >= 10, ������ �������� �������� �����
            {
                turbo = 2;
            }
            else if(AllEnemies.Length <= 3)//���� ������� ���� ����, ������ �������� �� ��������
            {
                turbo = 1;
            }
                GetComponent<Rigidbody>().velocity = turbo * speed * transform.forward;
        }
        else if(speed > 0) //���� ���� �� ��, � �������� � - �������� �� �������
        {
            speed -= 0.1f;
            if(speed < 0)
            {
                speed = 0;
            }
            GetComponent<Rigidbody>().velocity = speed * transform.forward;
        }
    }

    void OnCollisionEnter(Collision e)
    {
        if(e.gameObject.tag == "CubeEnemy") //���� ���������� ����, ������� ����, �������� ������� �� ��������� ��������� Particle system;
        {
            Destroy(e.gameObject);
            ScoreManager.Score += 1;
            Destroy(Instantiate(Effect, transform.position, transform.rotation), 2);
        }
    }
}
